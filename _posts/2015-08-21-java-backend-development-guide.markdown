---
layout: post
title:  "Java Backend Development Guide"
date:   2015-08-21 14:09:02
categories: Sever Development
comments: true
---

While working at Oracle, I have been learning and following applicable standards, best practices, and intelligently reusing code. It is the main collective development responsibility. In this blog post, I will describe some of the overall architecture of the Java backend I used.

<!--more-->

## Architectural Overview
![Alt text](/assets/img/serverJava.jpg)

The server runs entirely in a Weblogic 12c container. We are heavily dependent on the Spring Framework for dependency injection. In a nutshell, dependency injection wires up components. Beans are defined in a specific context and are scoped. More on that later.

## Webservice Resources

Our RESTful webservices use Jersey 1.18. Jersey is a JAX-RS reference implementation, meaning it implements specific high-level interfaces defined by the java community (in this case, javax.ws.rs). When ajax requests are sent to the server, anything matching "/rest" gets forwarded to Jersey, which then pairs the request up with one of our "Resource" files defined in the rest jar.  
In order to handle REST resource requests, payloads must be serialized into what we call "Resource Objects", or ROs for short. Serialization and deserialization happen automatically using Jackson 1.9.  

## Service Layer

After requests are handled and deserialized, we generally write service-layer logic. Service layer logic is traditionally encapsulated in "DAOs" (or Data Access Objects), though we are moving towards relabeling these as "Service" or "Action" objects. The responsibilities of a "Service" object are to take request ROs and perform some operation on them (eg: Updating data, loading data, etc...).  
Entities in Context discusses the use of DAOs, Services, and Resources in more depth.

## JPA/Rules Engine

In order to retrieve data, services should autowire the SecurableEntityManager and query for that data using named JPQL queries. JPQL is a special SQL-like language specified in the JPA specification. Data that is retrieved is served in the form of POJOs called "Entity Objects" or "Domain Objects". All EOs are defined in the model jar and implement an interface prescribed in the common jar. EOs are mapped to the database by the JPA specification. The JPA implementation we use is Eclipselink.  
After JPQL queries are executed, the results are filtered by the entity manager given the Session bean. So if the current user doesn't have view access to an object, it is removed so that user never sees it.  
In order to store new data or update existing data, services should autowire the RulesExecutor, which is responsible for running business rules on the objects that are persisted/merged. The RulesExecutor calculates changes on the Entity Object and invokes the corresponding RulesEngine for that type. (Note: If the data is a new record, the new entity must be created using an @Autowired EntityFactory instance – this is in order that security gets applied to the new entity properly).

## Akka

Finally, user actions are not the only thing that can make changes to objects in the system. You may want to have a scheduled process update data, or maybe you want an asynchronous calculation to occur after an entity has been persisted or updated. In these cases, we use a message-based system built on top of Akka.

<div class="container">

{% highlight javascript linenos %}
function Car( model ) {
	this.model = model;
	this.color = "silver";
	this.year  = "2012";
	this.getInfo = function () {
		return this.model + " " + this.year;
	};
}
{% endhighlight %}

Learning JavaScript Design Patterns is released under a [Creative Commons Attribution-Noncommercial-No Derivative Works 3.0](http://creativecommons.org/licenses/by-nc-nd/3.0/) unported license. It is available for purchase via [O'Reilly Media](http://www.oreilly.com/) but will remain available for both free online and as a physical (or eBook) purchase for readers wishing to support the project.