---
layout: post
title:  "Why Did Google Create Alphabet"
date:   2015-08-22 08:15:00
categories: Companies
comments: true
---

Alphabet Inc has a lot of parallels with how Berkshire Hathaway is structured and just like Warren Buffet, we now have to think of Larry and Sergey as investors, not entrepreneurs per se. Looking at them in this new light gives some ideas of why this was done.

<!--more-->

![Alt Alphabet](http://d2brer6wwumtdu.cloudfront.net/wp-content/uploads/2015/08/12121058/google-alphabet-logo-1439294822.jpg)

## Freedom of capital allotment 

Larry and Sergey now act as money managers and have larger freedom to assign substantial capital to any business they choose. Under Google, they would be subject to shareholder questions like "Why invest in Calico (a life longevity company)  when it has no demonstrable impact on ad revenues?" or "Is Project Loon (balloon based internet) really strategic to Google?" As shareholders of Alphabet, you are trusting their judgment as money managers and not as operators of any particular company.

## Taxation benefits

Under the new structure, Alphabet Inc. does not engage in any business operation, which enables the holding company to reduce the responsibility of owning a business. Alphabet Inc has no manufacturing, no products and no services. This allows it to maximize the profits and minimize taxation liability because the dividends it gets from subsidiaries are not taxed (if ownership >80%).

## Risk mitigation

New structure means that any individual company does not bear liabilities of other companies. This means Google Inc does not bear any liability for potential moonshot projects like Google X that might have substantial losses. Google Inc., becomes a predictable business with predicable growth/ revenues.

## Google Inc escapes quarterly scrutiny

Alphabet might not choose to declare the P&L accounts for Google Inc.,  in depth.

e.g Amazon did not explicitly mention the revenues and expenses of AWS as a business line until Q1 '15. We had to make assumptions/ predictions. Investors think that AWS can be highly valuable as a standalone business, which means that Amazon stock as a whole is undervalued. 

If this is true, that means as a company, Google has 
1. More freedom for R&D/ risky projects
2. Not to worry about quarterly results or shareholder perception.